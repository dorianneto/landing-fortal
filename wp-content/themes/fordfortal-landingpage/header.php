<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php bloginfo('description') ?>">
    <meta name="author" content="Dorian Neto">
    <link rel="icon" href="assets/img/favicon.ico">

    <title><?php wp_title(''); ?></title>

    <!-- Components styles for this template -->
    <link href="<?=CSS_PATH?>bootstrap.min.css" rel="stylesheet">
    <link href="<?=CSS_PATH?>font-awesome.min.css" rel="stylesheet">
    <link href="<?=CSS_PATH?>animate.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="<?=CSS_PATH?>main.min.css" rel="stylesheet">
    <link href="<?=THEME_PATH?>style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <?php wp_head(); ?>
</head>
<body>
    <header id="header">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-8">
                    <div id="logo">
                        <img src="<?=IMAGE_PATH?>logo-ford-fortal.png" alt="Ford Fortal">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <ul class="list">
                        <li><a href="tel:8534525950"><i class="fa fa-mobile fa-3x" aria-hidden="true"></i> (85) 3452-5950</a></li>
                        <li><a href="tel:85999691603"><i class="fa fa-whatsapp fa-2x" aria-hidden="true"></i> (85) 99969-1603</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </header>
