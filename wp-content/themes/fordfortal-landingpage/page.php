<?php get_header(); ?>

    <?php the_post(); ?>

    <main id="main">
        <div class="container">
            <div class="row">
                <section class="col-xs-12 col-sm-12 col-md-8 text">
                    <h1><?php the_title(); ?></h1>
                    <?php the_content(); ?>
                    <?php if (has_post_thumbnail()) : ?>
                        <figure class="animated bounceInLeft">
                            <?php the_post_thumbnail(); ?>
                        </figure>
                    <?php endif; ?>
                </section>
                <?php
                /**
                 * Formulário para conversão
                 */
                $form = get_post_meta($post->ID, 'formulario', true);

                if (!empty($form)) :
                ?>
                    <section class="col-xs-12 col-sm-12 col-md-4 form">
                        <h2>Estou<br><small>interessado</small></h2>
                        <?=do_shortcode($form)?>
                        <ul class="social list">
                            <li><a href="https://www.facebook.com/fordfortal" title="Curta nossa página" target="_blank"><img src="<?=IMAGE_PATH?>icon-facebook.png" title="Curta nossa página" alt="Facebook"></a></li>
                            <li><a href="https://www.instagram.com/fordfortal/" title="Siga no Instagram" target="_blank"><img src="<?=IMAGE_PATH?>icon-instagram.png" title="Siga no Instagram" alt="Instagram"></a></a></li>
                            <li><a href="http://www.fordfortal.com.br/" title="Visite nosso site" target="_blank">www.fordfortal.com.br</a></li>
                        </ul>
                    </section>
                <?php endif; ?>
            </div>
        </div>
    </main>

<?php get_footer(); ?>