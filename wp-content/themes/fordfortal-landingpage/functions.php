<?php
define('THEME_PATH', get_bloginfo('template_url') . '/');

define('IMAGE_PATH', THEME_PATH . 'assets/img/');
define('CSS_PATH', THEME_PATH . 'assets/css/');
define('JS_PATH', THEME_PATH . 'assets/js/');

// Theme support
add_theme_support( 'post-thumbnails' );

// Hooks
function my_the_content_filter($content) {
    $content = str_replace('<ul', '<ul class="list"', $content);

    return $content;
}
add_filter( 'the_content', 'my_the_content_filter' );