<?php get_header(); ?>

    <?php the_post(); ?>

    <main id="main">
        <div class="container">
            <div class="row">
                <section class="col-lg-12 text-center">
                    <h1>Erro 404</h1>
                    <p>Infelizmente, a página que você buscou não foi encontrada. Verifique se a url que você digitou está correta.</p>
                </section>
            </div>
        </div>
    </main>

<?php get_footer(); ?>